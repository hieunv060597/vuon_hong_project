(function ($) {
    window.onload = function () {        
        $(document).ready(function () {		
			back_to_top();
            moblie_bar();
            slider_client();
            open_search_hd();     
            stuck_header();   
            drop_list_in_cate_pages();  
            custom_op_cate();
            galerry_product();
            pop_up_form();
        });        
    };
})(jQuery);



function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function moblie_bar() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}

function slider_client(){
    $('.client-feed-back .owl-carousel').owlCarousel({
        loop:true,
        margin: 25,
        autoplay:true,
        nav: true,
        navText: ["<i class = 'fa fa-angle-left'></i>", "<i class = 'fa fa-angle-right'></i>"],
        dots: false,
        autoplayTimeout:6000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
        0:{
            items:1,           
        },
        576:{
           items:1,
        },
        768:{
           items:2,
        },
        1024:{
            items:2,
        },

        1400:{
           items:2,
        }
    }
  })
}

function open_search_hd(){
	var btn_op_search = document.querySelector('#mb-search');
	var over_lay_search = document.querySelector('.over-lay-mb');
	var box_search_hd = document.querySelector('.box-search_header');
	
	btn_op_search.onclick = function(){
		over_lay_search.classList.add('overlay-open');
		box_search_hd.classList.add('active_show');
	}
	
	over_lay_search.onclick = function(){
		over_lay_search.classList.remove('overlay-open');
		box_search_hd.classList.remove('active_show');
	}
}

function stuck_header(){
    var top_bar = document.querySelector('#header .top-bar');
    var header_main = document.querySelector('#header .header-main');
    var check = true;

    window.addEventListener('scroll', function(){
        if(window.pageYOffset > 260){
            if(check == true){
                top_bar.classList.add('off-top-bar');
                header_main.classList.add('fixed-stuck');
                check = false;
            }
        }
        else{
            if(check == false){
                top_bar.classList.remove('off-top-bar');
                header_main.classList.remove('fixed-stuck');
                check = true;
            }
        }
    })

}

function drop_list_in_cate_pages(){
    var li_has_drop = $('.main-cate .container.cate-2 .list-cate-box .list-cates .has-child');
    var ul_child_drop = $('.main-cate .container.cate-2 .list-cate-box .list-cates .drop-child');
    if(li_has_drop.length == 0 && ul_child_drop.length == 0){
        return 0;
    }

    else{
        ul_child_drop.after("<span class = 'btn-drop'></span>");
        var btn_drop = $('.main-cate .container.cate-2 .list-cate-box .list-cates .btn-drop');
    }

    for(var i = 0; i < btn_drop.length; i++){
        btn_drop[i].onclick = function(){
            this.classList.toggle('active');
            for(var k = 0; k < ul_child_drop.length; k++){
                var pannel = ul_child_drop[k].nextElementSibling;

                if(pannel.classList[1] == 'active'){
                    ul_child_drop[k].classList.add('show-drop');
                }
                else{
                    ul_child_drop[k].classList.remove('show-drop');
                }
            }

        }
    }
}

function custom_op_cate(){
    var box_drop = document.querySelector('.main-cate .cate-2 .row .has-custom-fix');
    var btn_drop = document.querySelector('#btn-open-cate-list');
    var overlay = document.querySelector('.overlay-f');

    if(box_drop == null && btn_drop == null && overlay == null){
        return 0;
    }
    else{
        btn_drop.onclick = function(){
            box_drop.classList.add('show-custom');
            overlay.classList.add('active-ov');
        }

        overlay.onclick = function(){
            box_drop.classList.remove('show-custom');
            overlay.classList.remove('active-ov');
        }
    }
}

function galerry_product(){
	$('#product-detail .infor-product .product-galerry .slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		arrows: true,
		fade: true,
		asNavFor: '.slider-nav',
		autoplay: true,
        autoplaySpeed: 5000,
	});

	$('#product-detail .infor-product .product-galerry .slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		autoplay: true,
        autoplaySpeed: 5000,
		dots: false,
		centerMode: true,
		centerPadding: '2px',
		arrows: true,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 1800,
				settings: {
				  slidesToShow: 5,
				}
			  },
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 5,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 4,
			  }
			},
			{
				breakpoint: 576,
				settings: {
				  slidesToShow: 3,
				}
			  },
			{
			  breakpoint: 320,
			  settings: {
				slidesToShow: 2,
			  }
			}
		]
	});
}

function pop_up_form(){
    var form_pop_up = document.querySelector('.form-pop-up .container');
    var btn_op_pop = document.querySelector('#op-pop-up-dt');
    var overlay = document.querySelector('.form-pop-up .overlay-pop-up');

    if(form_pop_up == null && btn_op_pop == null && overlay == null){
        return 0;
    }
    else{
        btn_op_pop.onclick = function(){
            overlay.classList.add('show-pop-ovl');
            form_pop_up.classList.add('show-up');
        }

        overlay.onclick = function(){
            overlay.classList.remove('show-pop-ovl');
            form_pop_up.classList.remove('show-up');
        }
    }
}

//loc gia----------------------------------------------------------

$( function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 255000,
        values: [ 9000 , 255000],
        slide: function( event, ui ) {
            $( "#amount" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
            $( "#price1" ).text(ui.values[ 0 ] +  "$");
            $( "#price2" ).text(ui.values[ 1 ] +  "$");
        }
    });
    $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 ));
    $( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 ));
    $( "#price1" ).text($( "#slider-range" ).slider( "values", 0 ) + " " + "$");
    $( "#price2" ).text($( "#slider-range" ).slider( "values", 1 ) + " " + "$");
} );



